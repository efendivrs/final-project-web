import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('Common Test Case/Login'), [('Email') : 'fatija3142@locawin.com', ('Password') : 'cZFrDSk31FeaspcjiMwZ6g=='],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Case/Add Event to Cart'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.check(findTestObject('Object Repository/Checkout Page/checkbox_Event Item'))

WebUI.click(findTestObject('Object Repository/Checkout Page/button_Checkout'))

WebUI.click(findTestObject('Object Repository/Checkout Page/radio_Payment Method'))

WebUI.click(findTestObject('Object Repository/Checkout Page/button_Confirm'))

WebUI.delay(GlobalVariable.G_Timeout)

WebUI.click(findTestObject('Object Repository/Page_Coding.ID - Cart/div_Creditdebit card'))

WebUI.setText(findTestObject('Object Repository/Page_Coding.ID - Cart/input_Card number_valid-input-value'), '4773775202011809')

WebUI.setText(findTestObject('Object Repository/Page_Coding.ID - Cart/input_Expiration date_card-expiry_1'), '0125')

WebUI.setText(findTestObject('Object Repository/Page_Coding.ID - Cart/input_CVV_card-cvv_1'), '123')

WebUI.click(findTestObject('Object Repository/Page_Coding.ID - Cart/button_Pay now'))

WebUI.takeScreenshotAsCheckpoint('payment_failed_status')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Coding.ID - Cart/div_Payment declined by bank'), 'Payment declined by bank')

WebUI.click(findTestObject('Object Repository/Page_Coding.ID - Cart/button_OK'))

WebUI.click(findTestObject('Object Repository/Page_Coding.ID - Cart/img'))

WebUI.click(findTestObject('Object Repository/Page_Coding.ID - Cart/button_Yes, cancel'))

WebUI.click(findTestObject('Object Repository/Page_Coding.ID - Cart/div_PT Dwidata Talenta Prima_close-snap-but_97236f'))

WebUI.delay(GlobalVariable.G_Timeout)

WebUI.navigateToUrl('https://demo-app.online/dashboard/invoice')

WebUI.click(findTestObject('Object Repository/Invoice Page/button_Detail'))

WebUI.verifyElementText(findTestObject('Object Repository/Invoice Page/text_Unpaid'), 'Unpaid')

WebUI.closeBrowser()