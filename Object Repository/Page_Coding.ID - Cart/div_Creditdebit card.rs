<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Creditdebit card</name>
   <tag></tag>
   <elementGuidId>3c72307a-d5ae-40ee-9808-3d9f7953cb2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div[3]/a/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.list-title.text-actionable-bold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f9848f31-0cc1-4b38-a247-4a2be5767070</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-title text-actionable-bold</value>
      <webElementGuid>59542ed3-084c-4b66-b010-3e59ea641583</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Credit/debit card</value>
      <webElementGuid>c6fdd863-d61c-439a-989f-7ea926bff6f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-container-list&quot;]/div[3]/a[@class=&quot;list&quot;]/div[@class=&quot;list-content&quot;]/div[@class=&quot;list-title text-actionable-bold&quot;]</value>
      <webElementGuid>8829c115-11ac-4957-9733-ea7de56c7240</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Coding.ID - Cart/iframe_concat(id(, , snap-midtrans, , ))_po_fe51e6</value>
      <webElementGuid>663a07b9-1924-400e-866f-9463aa2126c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div[3]/a/div/div</value>
      <webElementGuid>019a1957-74ec-4764-a0fa-6ce999a4b51a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other banks'])[1]/following::div[4]</value>
      <webElementGuid>dc5e8c5c-d00d-4324-a842-24fe11969308</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Virtual account'])[1]/following::div[32]</value>
      <webElementGuid>96b8b29a-8acf-4ed8-8e53-2fca6e367aba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ShopeePay'])[1]/preceding::div[8]</value>
      <webElementGuid>8f8e4f67-0e43-4ec8-b98c-2d3dd23abc8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret'])[1]/preceding::div[16]</value>
      <webElementGuid>95dbc57b-1b08-470f-9d36-937fb5e1efeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Credit/debit card']/parent::*</value>
      <webElementGuid>13d2a5b7-1e12-48f8-9173-6b932ade984b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a/div/div</value>
      <webElementGuid>f01331b5-0851-4557-9d99-e64da3e7466a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Credit/debit card' or . = 'Credit/debit card')]</value>
      <webElementGuid>d6f997d8-90bf-41ea-ad3a-6c221082d007</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
