<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_PT Dwidata Talenta Prima_close-snap-but_97236f</name>
   <tag></tag>
   <elementGuidId>325e0e6d-3ae1-433f-8053-2c74ead65a85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='header']/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.close-snap-button.clickable</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>876deec3-ea53-4617-8085-5e968e632953</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>close-snap-button clickable</value>
      <webElementGuid>f612f557-c53a-4bfe-9999-9c3ba5ae8f4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header&quot;)/div[@class=&quot;title-bar&quot;]/div[@class=&quot;close-snap-button clickable&quot;]</value>
      <webElementGuid>677ca895-f16c-4b31-9a60-76c9c6a40cc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Coding.ID - Cart/iframe_concat(id(, , snap-midtrans, , ))_po_fe51e6</value>
      <webElementGuid>cf9f6fa1-c312-43d1-9ec9-ba897c8a571b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='header']/div/div[2]</value>
      <webElementGuid>642614d0-8a46-430a-a37e-1c6329c04aca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PT Dwidata Talenta Prima'])[1]/following::div[1]</value>
      <webElementGuid>31b98116-73d7-4ff1-9fb3-186870436f36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEST'])[1]/following::div[6]</value>
      <webElementGuid>d58bb06b-c8c4-46fd-9867-e85596c13954</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/preceding::div[1]</value>
      <webElementGuid>0fb3ee8b-ec49-492e-a09f-4a8ca63449db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]</value>
      <webElementGuid>1d788078-410c-40dc-9a14-87f714dbff06</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
