<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Yes, cancel</name>
   <tag></tag>
   <elementGuidId>b844a003-b065-48c4-a3fa-037d28e98592</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.danger.short</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1706ee36-f940-4858-a883-a82c134ecaa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c5d954fb-8b00-4207-af23-a448b436990f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn danger short</value>
      <webElementGuid>fe216c0d-a2ad-4834-9d69-e4c55922e9d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Yes, cancel</value>
      <webElementGuid>c9ca36bb-077a-415b-b8ce-4173ebba6df2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;credit-card__wrapper&quot;]/div[@class=&quot;credit-card__content&quot;]/div[@class=&quot;cancel-modal-content&quot;]/span[@class=&quot;cancel-right-button&quot;]/button[@class=&quot;btn danger short&quot;]</value>
      <webElementGuid>95a57d3d-52ff-438d-a384-3ea272840b66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Coding.ID - Cart/iframe_concat(id(, , snap-midtrans, , ))_po_fe51e6</value>
      <webElementGuid>229c8df2-4b82-4fa5-a1c9-e4971167e7fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[2]</value>
      <webElementGuid>1a35b1b9-e639-4544-9c27-e37c8a8ba90a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div[6]/span[2]/button</value>
      <webElementGuid>c57cfe39-0dee-47db-b6af-e39eab74dfe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::button[1]</value>
      <webElementGuid>8282372e-5609-4a03-8b7e-7d449d191897</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If cancelled, all filled information will not be saved.'])[1]/following::button[2]</value>
      <webElementGuid>5eb73213-6dd6-428f-989c-e1a56e854e2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay now'])[1]/preceding::button[1]</value>
      <webElementGuid>87567683-ac05-432a-a388-3ca2600caa67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Yes, cancel']/parent::*</value>
      <webElementGuid>b8787904-0596-47fd-99d2-b5ffd35652e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/button</value>
      <webElementGuid>97a4128a-3a30-483e-8a88-c63883e6456f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Yes, cancel' or . = 'Yes, cancel')]</value>
      <webElementGuid>a62ef0fb-cb3e-4eec-a607-8ed003f4f357</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
