<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_OK</name>
   <tag></tag>
   <elementGuidId>9f565708-68d5-4bb9-9d53-36cb3233ef08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.full.primary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c6b4cc76-6f1e-42d6-ac2f-40508ad0c288</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>3e45a44e-894b-4713-947b-b2ff72d967e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full primary</value>
      <webElementGuid>daa5cb63-e30b-40c8-b877-1483f42ce26f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>7310c5e1-6b10-4bda-b201-eda9e2198594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;credit-card__wrapper&quot;]/div[@class=&quot;credit-card__content&quot;]/div[@class=&quot;cancel-modal-content&quot;]/button[@class=&quot;btn full primary&quot;]</value>
      <webElementGuid>3a005d8b-5147-4e3c-9e02-0cf0dbaf5618</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_Coding.ID - Cart/iframe_concat(id(, , snap-midtrans, , ))_po_fe51e6</value>
      <webElementGuid>48314629-3928-4217-a9bc-87c34440c203</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>29498be5-23a7-400d-83c8-256242044c4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div[6]/button</value>
      <webElementGuid>1e935f80-562c-46df-8c34-ce4117e8fb7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please use another card or change payment method. (Error code: DECLINED)'])[1]/following::button[1]</value>
      <webElementGuid>31e63fb0-745c-4fd4-b4a1-7b7dcd80d7f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment declined by bank'])[1]/following::button[1]</value>
      <webElementGuid>7070848e-632b-4253-ae99-1bdc73a23e27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay now'])[1]/preceding::button[1]</value>
      <webElementGuid>d1286068-4932-4225-81d8-d00236f5d6c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OK']/parent::*</value>
      <webElementGuid>94da68e6-add3-4762-af5b-d48808a3ba50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>e7e7c8e9-7c8d-419d-93c5-d10f40134cff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'OK' or . = 'OK')]</value>
      <webElementGuid>1a24674f-cba0-45cf-b4e5-e04439cc9481</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
