<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>radio_Payment Method</name>
   <tag></tag>
   <elementGuidId>8a3e67b4-b661-4253-a77e-8e315634d05f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='bank']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#bank</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2ba1f6f1-9f55-4b59-80b0-ca359f7ad929</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>ec1ca2de-7cc1-4c48-87d7-6fd06a091d78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control-inline payment</value>
      <webElementGuid>7c9d8b38-d838-4b5d-bc7b-e3235a13cf22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>payment_method</value>
      <webElementGuid>c4e9d4d7-e0ac-4430-8320-f7c46cf8300d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>e-wallet</value>
      <webElementGuid>8f1ccf4e-1130-44f1-b149-24e8e8c44a28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>bank</value>
      <webElementGuid>cc9daa73-b539-4475-b4ae-b1ba1e7b75c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bank&quot;)</value>
      <webElementGuid>81a7fcec-786a-481e-be95-3320aa160e7d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='bank']</value>
      <webElementGuid>9fba7dde-1dce-4411-ad66-fcb3b60f5bb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table[2]/tbody/tr/td/input</value>
      <webElementGuid>59d8a6ee-fd07-4ddc-8a57-8340d3e58874</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/input</value>
      <webElementGuid>c814bebb-cd79-444e-9601-90ff813cc714</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'payment_method' and @id = 'bank']</value>
      <webElementGuid>f6b0583d-80ae-4435-8f22-347a84f80896</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
